call plug#begin('~/.local/share/nvim/plugged')

Plug 'zah/nim.vim'
"Plug 'fenetikm/falcon'
Plug 'majutsushi/tagbar'
Plug 'xolox/vim-easytags'
Plug 'xolox/vim-misc'
Plug '/usr/share/fzf'
Plug 'junegunn/fzf.vim'
Plug 'freeo/vim-kalisi'
Plug 'mhinz/vim-startify'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'mhinz/vim-grepper'
Plug 'dart-lang/dart-vim-plugin'
Plug 'scrooloose/nerdcommenter'
Plug 'aradunovic/perun.vim'
Plug 'airblade/vim-rooter'
Plug 'morhetz/gruvbox'
Plug 'terryma/vim-multiple-cursors'
"Plug 'ajmwagar/vim-deus'
"Plug 'Badacadabra/vim-archery'

call plug#end()

set number
"colorscheme falcon
colorscheme gruvbox
set completeopt=longest,menuone
let mapleader=" "
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
nmap <F7> :GrepperRg -i -g "!tags" 
nmap <F8> :TagbarToggle<CR>
"nmap <F3> <c-]>
nmap <F3> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
nmap <F9> :CtrlPBufTag<CR>
nnoremap <c-j> <c-d>
nnoremap <c-k> <c-u>
nmap <F5> :Startify<CR>
nmap <F6> gg/^import\><CR>VNzf:noh<CR>
let g:rooter_manual_only = 1
nmap <Leader>f :cd ~/workspace<CR>:FZF<CR>
nmap <F4> :30Vex<CR>
set background=dark
set tabstop=4
set shiftwidth=4

if $SSH_CONNECTION
	colorscheme desert
endif
