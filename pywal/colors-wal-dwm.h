static const char norm_fg[] = "#d8d0cc";
static const char norm_bg[] = "#151214";
static const char norm_border[] = "#97918e";

static const char sel_fg[] = "#d8d0cc";
static const char sel_bg[] = "#993031";
static const char sel_border[] = "#d8d0cc";

static const char urg_fg[] = "#d8d0cc";
static const char urg_bg[] = "#625759";
static const char urg_border[] = "#625759";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
