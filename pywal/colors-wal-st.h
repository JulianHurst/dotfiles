const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#151214", /* black   */
  [1] = "#625759", /* red     */
  [2] = "#993031", /* green   */
  [3] = "#B04E47", /* yellow  */
  [4] = "#A35D59", /* blue    */
  [5] = "#BB955F", /* magenta */
  [6] = "#697391", /* cyan    */
  [7] = "#d8d0cc", /* white   */

  /* 8 bright colors */
  [8]  = "#97918e",  /* black   */
  [9]  = "#625759",  /* red     */
  [10] = "#993031", /* green   */
  [11] = "#B04E47", /* yellow  */
  [12] = "#A35D59", /* blue    */
  [13] = "#BB955F", /* magenta */
  [14] = "#697391", /* cyan    */
  [15] = "#d8d0cc", /* white   */

  /* special colors */
  [256] = "#151214", /* background */
  [257] = "#d8d0cc", /* foreground */
  [258] = "#d8d0cc",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
