#!/bin/sh
if [ "${TERM:-none}" = "linux" ]; then
    printf "%b" "\\e]P0151214"
    printf "%b" "\\e]P1625759"
    printf "%b" "\\e]P2993031"
    printf "%b" "\\e]P3B04E47"
    printf "%b" "\\e]P4A35D59"
    printf "%b" "\\e]P5BB955F"
    printf "%b" "\\e]P6697391"
    printf "%b" "\\e]P7d8d0cc"
    printf "%b" "\\e]P897918e"
    printf "%b" "\\e]P9625759"
    printf "%b" "\\e]PA993031"
    printf "%b" "\\e]PBB04E47"
    printf "%b" "\\e]PCA35D59"
    printf "%b" "\\e]PDBB955F"
    printf "%b" "\\e]PE697391"
    printf "%b" "\\e]PFd8d0cc"

    # Fix artifacting.
    clear
fi
